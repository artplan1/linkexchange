﻿using LinkExchange.Models;
using RomanPushkin.BetterRobokassa;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LinkExchange.Controllers
{
    public class MoneyController : Controller
    {
        private HelpContext db = new HelpContext();
        private UserProfile us;
        public ActionResult Index(string price, RobokassaConfirmationRequest confirmationRequest)
        {
            us = (UserProfile) db.UserProfiles.Where(u => u.UserName == User.Identity.Name).First();
            ViewBag.balance = us.Balance;

            if (string.IsNullOrEmpty(price))
            {
                if (confirmationRequest.InvId>0)
                {
                    
                        if (!string.IsNullOrEmpty(confirmationRequest.SignatureValue))
                        {
                            processOrder(confirmationRequest);
                            return Redirect("~/Money?success=true");
                        }
                        else
                        {
                            return Redirect("~/Money?success=false");
                        }
                    
                }
                return View();
            }
            else
            {
                string redirectUrl = Robokassa.GetRedirectUrl(int.Parse(price), us.UserId);
                return Redirect(redirectUrl);
            }
        }

        private void processOrder(RobokassaConfirmationRequest confirmationRequest)
        {
            if (db.InOperations.Where(op => op.TranId == confirmationRequest.SignatureValue).Count() == 0) {
                us.Balance += double.Parse(confirmationRequest.OutSum, CultureInfo.InvariantCulture);
                db.Entry(us).State = EntityState.Modified;

                var op = new InOp();
                op.userId = us.UserId;
                op.TranId = confirmationRequest.SignatureValue;
                op.sum = double.Parse(confirmationRequest.OutSum, CultureInfo.InvariantCulture);
                op.time = DateTime.Now;
                db.InOperations.Add(op);

                db.SaveChanges();
            }

        }

    }
}
