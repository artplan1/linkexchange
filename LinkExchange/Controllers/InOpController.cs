﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using LinkExchange.Models;

namespace LinkExchange.Controllers
{
    public class InOpController : ApiController
    {
        private HelpContext db = new HelpContext();

        // GET api/InOp
        public IEnumerable<InOp> GetInOps()
        {
            var inoperations = db.InOperations.Include(i => i.user).Where(op => op.user.UserName == User.Identity.Name);
            return inoperations.AsEnumerable();
        }

        // GET api/InOp/5
        public InOp GetInOp(int id)
        {
            InOp inop = db.InOperations.Find(id);
            if (inop == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return inop;
        }

        // PUT api/InOp/5
        public HttpResponseMessage PutInOp(int id, InOp inop)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            if (id != inop.Id)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            db.Entry(inop).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        // POST api/InOp
        public HttpResponseMessage PostInOp(InOp inop)
        {
            if (ModelState.IsValid)
            {
                db.InOperations.Add(inop);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, inop);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = inop.Id }));
                return response;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        // DELETE api/InOp/5
        public HttpResponseMessage DeleteInOp(int id)
        {
            InOp inop = db.InOperations.Find(id);
            if (inop == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            db.InOperations.Remove(inop);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, inop);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}