﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LinkExchange.Controllers
{
    public class WebmasterController : Controller
    {
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }
    }
}
