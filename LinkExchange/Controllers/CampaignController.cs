﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using LinkExchange.Models;

namespace LinkExchange.Controllers
{
    public class CampaignController : ApiController
    {
        private HelpContext db = new HelpContext();

        // GET api/Campaign
        public IEnumerable<Campaign> GetCampaigns()
        {
            var campaignes = db.Campaignes.Include(c => c.AdvType).Include(c => c.Category);
            return campaignes.AsEnumerable();
        }

        // GET api/Campaign/5
        public Campaign GetCampaign(int id)
        {
            Campaign campaign = db.Campaignes.Find(id);
            if (campaign == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return campaign;
        }

        // PUT api/Campaign/5
        public HttpResponseMessage PutCampaign(int id, Campaign campaign)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            if (id != campaign.CampaignId)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            db.Entry(campaign).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, campaign);
        }

        // POST api/Campaign
        public HttpResponseMessage PostCampaign(Campaign campaign)
        {
            if (ModelState.IsValid)
            {
                db.Campaignes.Add(campaign);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, campaign);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = campaign.CampaignId }));
                return response;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        // DELETE api/Campaign/5
        public HttpResponseMessage DeleteCampaign(int id)
        {
            Campaign campaign = db.Campaignes.Find(id);
            if (campaign == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            db.Campaignes.Remove(campaign);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, campaign);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}