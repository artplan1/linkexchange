﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using LinkExchange.Models;
using LinkExchange.Filters;
using System.Web.Mvc;
using System.Net.Http.Headers;
using Newtonsoft.Json;

namespace LinkExchange.Controllers
{
    //[Authorize]
    //[ValidateHttpAntiForgeryToken]
    public class AdvPlaceController : ApiController
    {
        private HelpContext db = new HelpContext();

        // GET api/AdvPlace
        public IEnumerable<AdvPlace> GetAdvPlaces(int t)
        {
            IEnumerable<AdvPlace> advplaces;
            if(t==0)
                advplaces = db.AdvPlaces.Include(a => a.AdvType).Include(a => a.Category).Where(u => u.UserName == User.Identity.Name);
            else
                advplaces = db.AdvPlaces.Include(a => a.AdvType).Include(a => a.Category).Take(10);
            return advplaces.AsEnumerable();
        }

        public IEnumerable<AdvPlace> GetAdvPlaces(int type, int cat) {
            IEnumerable<AdvPlace> advplaces = db.AdvPlaces.Include(a => a.AdvType).Include(a => a.Category).Where(t => t.CategoryId == cat && t.AdvTypeId == type);
            return advplaces.AsEnumerable();
        }

        public HttpResponseMessage GetAdvPlaces(string url)
        {
            var cont = new WebClient().DownloadString("http://api.pr-cy.ru/analysis.json?domain=" + url);
            StringContent sc = new StringContent(cont);
            sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            HttpResponseMessage resp = new HttpResponseMessage();
            resp.Content = sc;

            return resp;
        }

        public int GetAdvPlaces(bool active)
        {
            int c = 0;
            if(active)
                c = db.AdvPlaces.Count(u => u.UserName == User.Identity.Name && u.IsActive == true);
            else
                c = db.AdvPlaces.Count(u => u.UserName == User.Identity.Name);
            return c;
        }

        // GET api/AdvPlace/5
        public AdvPlace GetAdvPlace(int id)
        {
            AdvPlace advplace = db.AdvPlaces.Find(id);
            if (advplace == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            if (advplace.UserName != User.Identity.Name)
            {
                // Попытка изменить запись, которая не принадлежит пользователю
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.Unauthorized));
            }

            return advplace;
        }

        // PUT api/AdvPlace/5
        public HttpResponseMessage PutAdvPlace(int id, AdvPlace advplace)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            if (id != advplace.AdvPlaceId)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            if (db.Entry(advplace).Entity.UserName != User.Identity.Name)
            {
                return Request.CreateResponse(HttpStatusCode.Unauthorized);
            }

            

            db.Entry(advplace).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, advplace);
        }

        // POST api/AdvPlace
        public HttpResponseMessage PostAdvPlace(AdvPlace advplace)
        {
            if (ModelState.IsValid)
            {
                advplace.UserName = User.Identity.Name;
                db.AdvPlaces.Add(advplace);
                db.SaveChanges();

                if (advplace.AdvTypeId == 1)
                {
                    var cont = new WebClient().DownloadString("http://api.pr-cy.ru/analysis.json?domain=" + url);
                    var jsonData = JsonConvert.DeserializeObject<dynamic>(cont);
                }

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, advplace);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = advplace.AdvPlaceId }));
                return response;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        // DELETE api/AdvPlace/5
        public HttpResponseMessage DeleteAdvPlace(int id)
        {
            AdvPlace advplace = db.AdvPlaces.Find(id);
            if (advplace == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            if (db.Entry(advplace).Entity.UserName != User.Identity.Name)
            {
                // Попытка изменить запись, которая не принадлежит пользователю
                return Request.CreateResponse(HttpStatusCode.Unauthorized);
            }
            db.AdvPlaces.Remove(advplace);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, advplace);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}