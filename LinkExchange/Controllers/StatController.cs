﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using LinkExchange.Models;

namespace LinkExchange.Controllers
{
    public class StatController : ApiController
    {
        private HelpContext db = new HelpContext();

        // GET api/Stat
        public IEnumerable<WmStat> GetWmStats()
        {
            return db.WmStats.Where(u => u.user == User.Identity.Name).AsEnumerable();
        }
    }
}