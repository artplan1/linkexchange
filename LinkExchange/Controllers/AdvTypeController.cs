﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using LinkExchange.Models;
using LinkExchange.Filters;

namespace LinkExchange.Controllers
{
    [Authorize]
    [ValidateHttpAntiForgeryToken]
    public class AdvTypeController : ApiController
    {
        private HelpContext db = new HelpContext();

        // GET api/AdvType
        public IEnumerable<AdvType> GetAdvTypes()
        {
            return db.AdvTypes.AsEnumerable();
        }

        // GET api/AdvType/5
        public AdvType GetAdvType(int id)
        {
            AdvType advtype = db.AdvTypes.Find(id);
            if (advtype == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return advtype;
        }

        // PUT api/AdvType/5
        public HttpResponseMessage PutAdvType(int id, AdvType advtype)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            if (id != advtype.AdvTypeId)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            db.Entry(advtype).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        // POST api/AdvType
        public HttpResponseMessage PostAdvType(AdvType advtype)
        {
            if (ModelState.IsValid)
            {
                db.AdvTypes.Add(advtype);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, advtype);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = advtype.AdvTypeId }));
                return response;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        // DELETE api/AdvType/5
        public HttpResponseMessage DeleteAdvType(int id)
        {
            AdvType advtype = db.AdvTypes.Find(id);
            if (advtype == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            db.AdvTypes.Remove(advtype);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, advtype);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}