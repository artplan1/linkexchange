﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace LinkExchange.Models
{
    [Table("AdvType")]
    public class AdvType
    {
        public int AdvTypeId { get; set; }

        [Required]
        public string Title { get; set; }

    }

    public class HelpContext : DbContext
    {
        public HelpContext() : base("DefaultConnection")
        {
        }

        public DbSet<AdvPlace> AdvPlaces { get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<AdvType> AdvTypes { get; set; }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Campaign> Campaignes { get; set; }
        public DbSet<WmStat> WmStats { get; set; }

        public DbSet<InOp> InOperations { get; set; }
    }

    [Table("Category")]
    public class Category
    {
        public int CategoryId { get; set; }

        [Required]
        public string Title { get; set; }

    }
}