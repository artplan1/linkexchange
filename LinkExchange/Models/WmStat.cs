﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LinkExchange.Models
{
    [Table("WmStat")]
    public class WmStat
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string user { get; set; }
        public DateTime time { get; set; }
        public double count { get; set; }
    }
}