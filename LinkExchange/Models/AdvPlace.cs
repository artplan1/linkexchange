﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace LinkExchange.Models
{
    [Table("AdvPlace")]
    public class AdvPlace
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int AdvPlaceId { get; set; }

        [Required]
        public string Title { get; set; }
        public bool IsActive { get; set; }

        public string UserName { get; set; }

        [ForeignKey("AdvType")]
        public int AdvTypeId { get; set; }
        public virtual AdvType AdvType { get; set; }

        [ForeignKey("Category")]
        public int CategoryId { get; set; }
        public virtual Category Category { get; set; }
    }
}