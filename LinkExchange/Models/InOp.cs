﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LinkExchange.Models
{
    [Table("InOps")]
    public class InOp
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string TranId { get; set; }
        public double sum { get; set; }

        public DateTime time { get; set; }

        [Column("user")]
        public int userId { get; set; }

        public virtual UserProfile user { get; set; }

    }
}