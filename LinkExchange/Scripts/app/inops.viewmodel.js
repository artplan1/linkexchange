﻿function ajaxRequest(url, type, data) {
    var options = {
        dataType: "json",
        contentType: "application/json",
        error: function (jqXHR, textStatus, errorThrown) {
            humane.log("Ошибка: " + errorThrown);
        },
        type: type,
        data: data ? ko.toJSON(data) : null
    },
        antiForgeryToken = $("#antiForgeryToken").val();

    if (antiForgeryToken) {
        options.headers = {
            'RequestVerificationToken': antiForgeryToken
        }
    }
    return $.ajax(window.location.origin + url, options);
}

var Ops = function () {
    "use strict";
    var self = this;

    self.Ops = ko.observableArray();
 
	function GetOps() {
	    return ajaxRequest("/api/inop","get").done(function (data) { self.Ops(data)});
	}
    
	GetOps();
}

var vm = new Ops();
ko.applyBindings(vm);

$(function () {
    var parameter = window.location.search.replace("?", "");
    var values = parameter.split("=");
    
    if (values[1] == "true") {
        var su = humane.create({ baseCls: 'humane-flatty', addnCls: 'humane-flatty-success' });
        su.log("Баланс пополнен");
    }
    if (values[1] == "false") {
        var err = humane.create({ baseCls: 'humane-flatty', addnCls: 'humane-flatty-error' });
        err.log("Произошла ошибка. Баланс не пополнен.");
    }
});


