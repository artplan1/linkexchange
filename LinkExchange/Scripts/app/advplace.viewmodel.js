﻿function ajaxRequest(url, type, data) {
    var options = {
        dataType: "json",
        contentType: "application/json",
        error: function (jqXHR, textStatus, errorThrown) {
            humane.log("Ошибка: " + errorThrown);
        },
        type: type,
        data: data ? ko.toJSON(data) : null
    },
        antiForgeryToken = $("#antiForgeryToken").val();

    if (antiForgeryToken) {
        options.headers = {
            'RequestVerificationToken': antiForgeryToken
        }
    }
    return $.ajax(window.location.origin + url, options);
}

var line;
function draw() {
    ajaxRequest("/api/Stat", "get").success(function (data) {
        line = new Morris.Line({

            element: 'chart',
            hideHover: 'auto',
            data: data,
            resize: true,
            xkey: 'time',

            ykeys: ['count'],

            labels: ['Доход'],
            dateFormat: function (x) { return new Date(x).toDateString() },
        });
        
    });
}

draw();

var AdvPlace = function () {
    "use strict";
    var self = this;
    self.chosenPlaces = ko.observable(false);
    self.chosenOrder = ko.observable(false);
    self.chosenPanel = ko.observable(true);
    self.isLoading = ko.observable(true);
    self.nullChart = ko.observable(false);
    self.title = ko.observable('');
    self.advTypeId = ko.observable('');
    self.categoryId = ko.observable('');
    self.advPlaceId = ko.observable('');
    self.username = ko.observable();

    var AdvPlace = {
        AdvPlaceId: self.advPlaceId,
        Title: self.title,
        AdvTypeId: self.advTypeId,
        CategoryId: self.categoryId,
        UserName: self.username
    },
    iAdvPlace = {
        Title: self.title,
        AdvTypeId: self.advTypeId,
        CategoryId: self.categoryId
    };

    self.AdvPlaces = ko.observableArray();
    self.Cats = ko.observableArray();
    self.Types = ko.observableArray();

    self.placecount = ko.observable();
    self.placeactive = ko.observable();

    self.url = ko.observable();

    self.Cy = ko.observable();
    self.PR = ko.observable();
    self.crDate = ko.observable();
    self.exDate = ko.observable();
    self.gindex = ko.observable();
    self.yindex = ko.observable();

	function GetAdvPlaces() {
	    return ajaxRequest("/api/advPlace?t=0","get").done(function (data) { self.AdvPlaces(data);self.isLoading(false) });
	}

	function GetCount() {
	    ajaxRequest("/api/advPlace?active=false", "get").done(function (data) { self.placecount(data); });
	    ajaxRequest("/api/advPlace?active=true", "get").done(function (data) { self.placeactive(data); });
	}

	function GetCats() {
	    if(self.Cats().length>0){
	        return;
	    }
	    else{
	        return ajaxRequest('/api/Category/',"get").done(function (data) { self.Cats(data) });
	    }
	}

	function GetTypes() {
	    if (self.Types().length > 0) {
	        return;
	    }
	    else {
	        return ajaxRequest('/api/advtype/', "get").done(function (data) { self.Types(data) });
	    }
	}

	self.check = function () {
	    return ajaxRequest('/api/advplace?url=' + self.url().replace('http://',''), "get").done(function (data) {
	        if (data.error == null) {
	            self.Cy(data.stats.yandexCitation);
	            self.PR(data.stats.pageRank);
	            self.crDate(data.stats.creationDate);
	            self.exDate(data.stats.expirationDate);
	            self.gindex(data.stats.googleIndex);
	            self.yindex(data.stats.yandexIndex);
	        }
	        else {
	            self.Cy(-1);
	            var err = humane.create({ baseCls: 'humane-flatty', addnCls: 'humane-flatty-error' });
                if(data.error == 'SITE_NOT_FOUND_ERROR')
	            err.log("Произошла ошибка. "+data.message);
	        }
	    });
	}

	self.update = function () {
	    if (self.advPlaceId()) {
	        return ajaxRequest("/api/advPlace/" + self.advPlaceId(), "put", AdvPlace).done(function () { GetAdvPlaces(); $("#formEdit").modal('hide'); });
	    }
	    else {
	        return ajaxRequest("/api/advPlace/", "post", iAdvPlace).done(function () { GetAdvPlaces(); $("#formEdit").modal('hide'); });
	    }
	    
	};

	self.delete = function (advplace) {
	    return ajaxRequest("/api/advPlace/"+advplace.advPlaceId, "delete").done(function () { GetAdvPlaces() });
	}

	
	self.select = function (advplace) {
	    $("#editBtn").val("Редактировать");
	    self.username(advplace.userName);
	    self.advPlaceId(advplace.advPlaceId);
	    self.title(advplace.title);
	    self.categoryId(advplace.categoryId);
	    self.advTypeId(advplace.advTypeId);
	};

	$('#formEdit').on('hidden.bs.modal', function () {
	    $("#editBtn").val("Добавить");
	    self.advPlaceId(null);
	    self.advTypeId(null);
	    self.categoryId(null);
	    self.title(null);
	})

	self.places = function () {
	    GetCats();
	    GetTypes();
	    GetAdvPlaces();
	    $("#panel").removeAttr("class");
	    $("#order").removeAttr("class");
	    $("#places").addClass("active");
	    self.chosenPanel(false);
	    self.chosenPlaces(true);
	    self.chosenOrder(false);
	}

	self.panel = function () {
	    
	    $("#places").removeAttr("class");
	    $("#order").removeAttr("class");
	    $("#panel").addClass("active");
	    draw();
	    self.chosenPanel(true);
	    self.chosenPlaces(false);
	    self.chosenOrder(false);
	}

	self.order = function () {
	    $("#panel").removeAttr("class");
	    $("#places").removeAttr("class");
	    $("#order").addClass("active");
	    self.chosenPanel(false);
	    self.chosenPlaces(false);
	    self.chosenOrder(true);
	}
	GetCount();
	
}

var vm = new AdvPlace();
ko.applyBindings(vm);

$(document).ready(function () {

    $("#checkform").validate({

        rules: {

            url: {
                required: true,
                minlength: 4,
                maxlength: 16,
            },
        },

        messages: {

            url: {
                required: "Это поле обязательно для заполнения",
                minlength: "Логин должен быть минимум 4 символа",
                maxlength: "Максимальное число символо - 16",
            },

        }

    });

});