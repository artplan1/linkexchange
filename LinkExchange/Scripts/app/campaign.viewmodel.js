﻿var Campaign = function () {
    "use strict";
    var self = this;
    self.chosenCampaigns = ko.observable(false);
    self.chosenPanel = ko.observable(true);
    self.chosenSearch = ko.observable(false);
    self.isLoading = ko.observable(true);
    self.title = ko.observable('');
    self.advTypeId = ko.observable('');
    self.categoryId = ko.observable('');
    self.campaignId = ko.observable('');
    self.username = ko.observable();

    self.AdvPlaces = ko.observableArray();

    var Campaign = {
        CampaignId: self.campaignId,
        Title: self.title,
        AdvTypeId: self.advTypeId,
        CategoryId: self.categoryId,
        UserName: self.username
    },
    iCampaign = {
        Title: self.title,
        AdvTypeId: self.advTypeId,
        CategoryId: self.categoryId
    };

    self.Campaigns = ko.observableArray();
    self.Cats = ko.observableArray();
    self.Types = ko.observableArray();

    function ajaxRequest(url, type, data) {
        var options = {
            dataType: "json",
            contentType: "application/json",
            error: function (jqXHR, textStatus, errorThrown) {
                humane.log("Ошибка: " + errorThrown);
            },
            type: type,
            data: data ? ko.toJSON(data) : null
        },
            antiForgeryToken = $("#antiForgeryToken").val();

        if (antiForgeryToken) {
            options.headers = {
                'RequestVerificationToken': antiForgeryToken
            }
        }
        return $.ajax(window.location.origin + url, options);
	}

	function GetCampaigns() {
	    return ajaxRequest("/api/campaign/","get").done(function (data) { self.Campaigns(data);self.isLoading(false) });
	}

	function GetCats() {
	    if(self.Cats().length>0){
	        return;
	    }
	    else{
	        return ajaxRequest('/api/Category/',"get").done(function (data) { self.Cats(data) });
	    }
	}

	function GetTypes() {
	    if (self.Types().length > 0) {
	        return;
	    }
	    else {
	        return ajaxRequest('/api/advtype/', "get").done(function (data) { self.Types(data) });
	    }
	}

	self.update = function () {
	    if (self.campaignId()) {
	        return ajaxRequest("/api/campaign/" + self.campaignId(), "put", Campaign).done(function () { GetCampaigns(); $("#formEdit").modal('hide'); });
	    }
	    else {
	        return ajaxRequest("/api/campaign/", "post", iCampaign).done(function () { GetCampaigns(); $("#formEdit").modal('hide'); });
	    }
	    
	};

	self.delete = function (campaign) {
	    return ajaxRequest("/api/campaign/"+campaign.campaignId, "delete").done(function () { GetCampaigns() });
	}

	
	self.select = function (campaign) {
	    $("#editBtn").val("Редактировать");
	    self.username(campaign.userName);
	    self.campaignId(campaign.campaignId);
	    self.title(campaign.title);
	    self.categoryId(campaign.categoryId);
	    self.advTypeId(campaign.advTypeId);
	};

	$('#formEdit').on('hidden.bs.modal', function () {
	    $("#editBtn").val("Добавить");
	    self.campaignId(null);
	    self.advTypeId(null);
	    self.categoryId(null);
	    self.title(null);
	})

	

	self.campaigns = function () {
	    GetCats();
	    GetTypes();
	    GetCampaigns();
	    $("#panel").removeAttr("class");
	    $("#order").removeAttr("class");
	    $("#search").removeAttr("class");
	    $("#campaign").addClass("active");
	    self.chosenPanel(false);
	    self.chosenSearch(false);
	    //self.chosenOrders(false);
	    self.chosenCampaigns(true);
	}

	self.panel = function () {
	    $("#campaign").removeAttr("class");
	    $("#order").removeAttr("class");
	    $("#campaign").removeAttr("class");
	    $("#search").removeAttr("class");
	    $("#panel").addClass("active");
	    self.chosenSearch(false);
	    self.chosenCampaigns(false);
	    //self.chosenOrders(false);
	    self.chosenPanel(true);
	}

	self.order = function () {
	    $("#panel").removeAttr("class");
	    $("#campaign").removeAttr("class");
	    $("#search").removeAttr("class");
	    $("#order").addClass("active");
	    self.chosenPanel(false);
	    self.chosenSearch(false);
	    self.chosenCampaigns(false);
	    //self.chosenOrders(true);
	}

	self.search = function () {
	    $("#panel").removeAttr("class");
	    $("#campaign").removeAttr("class");
	    $("#order").removeAttr("class");
	    $("#search").addClass("active");
	    self.chosenPanel(false);
	    self.chosenCampaigns(false);
	    //self.chosenOrders(false);
	    self.chosenSearch(true);
	    GetAdvPlaces();
	    GetCats();
	    GetTypes();
	}

	self.searchPlaces = function () {
	    return ajaxRequest("/api/advplace?type="+self.advTypeId()+"&cat="+self.categoryId(), "get").done(function(data) { self.AdvPlaces(data);self.isLoading(false) });
	}

	function GetAdvPlaces() {
	    return ajaxRequest("/api/advPlace?t=1", "get").done(function (data) { self.AdvPlaces(data); self.isLoading(false) });
	}

}

ko.applyBindings(new Campaign());
