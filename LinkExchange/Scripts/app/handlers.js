﻿ko.bindingHandlers.loadingWhen = {
    init: function (element) {
        var
            $element = $(element),
            currentPosition = $element.css("position")
        $loader = $("<div>").addClass("spinner").hide();

        $element.append($loader);

        if (currentPosition == "auto" || currentPosition == "static")
            $element.css("position", "relative");

        $loader.css({
            top: "50%",
            left: "50%",
            "margin-left": -($loader.width() / 2) + "px",
            "margin-top": -($loader.height() / 2) + "px"
        });
    },
    update: function (element, valueAccessor) {
        var isLoading = ko.utils.unwrapObservable(valueAccessor()),
            $element = $(element),
            $childrenToHide = $element.children(":not(div.spinner)"),
            $loader = $element.find("div.spinner");

        if (isLoading) {
            $childrenToHide.css("visibility", "hidden").attr("disabled", "disabled");
            $loader.show();
        }
        else {
            $loader.fadeOut("fast");
            $childrenToHide.css("visibility", "visible").removeAttr("disabled");
        }
    }
};

